import { MouseEventHandler, useState } from 'react';
import './App.css';
import { LazyImage } from './components/LazyImage';

/** Generate a Random Function */
const random = (): number => Math.floor(Math.random() * 123 + 1);

const generarIDNumerico = (): string =>
	Array.from({ length: 4 }, () => Math.floor(Math.random() * 10)).join('');

const App = () => {
	/** Array de imagenes */
	const [images, setImages] = useState<IImageItem[]>([]);

	const addNewFox: MouseEventHandler<HTMLButtonElement> = (event) => {
		event.preventDefault();
		const newImageItem: IImageItem = {
			id: generarIDNumerico(),
			url: `https://randomfox.ca/images/${random()}.jpg`,
		};

		setImages([...images, newImageItem]);
    window.plausible('add_fox')
	};

	return (
		<>
			<h1 className="text-3xl font-bold underline">Hello world!</h1>
			<button onClick={addNewFox}> Add New Fox</button>
			{images.map(({id, url}, index) => (
				<div key={id} className="p-4">
					<LazyImage
						width={320}
						height={'auto'}
						className="rounded-lg bg-gray-300"
						src={url}
            onClick={() => console.log('hey')}
            onLazyLoad={(img) => {
              console.log(`Image #${index + 1} cargada. Nodo:`, img);
            }}
					/>
				</div>
			))}
		</>
	);
};

export default App;
