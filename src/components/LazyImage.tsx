import { useEffect, useRef, useState } from 'react';
import type {ImgHTMLAttributes} from 'react';

interface Props extends ImgHTMLAttributes<HTMLImageElement> {
    src: string
    onLazyLoad?: (img: HTMLImageElement) => void
}

export const LazyImage = ({ 
    src, 
    onLazyLoad,
    ...imgProps }: Props): JSX.Element => {

	const node = useRef<HTMLImageElement>(null);
	const [currentSrc, setCurrentSrc] = useState<string>('https://placehold.co/600x400');

	useEffect(() => {
		//nuevo observador
		const observer = new IntersectionObserver((entries) => {
			entries.forEach((entry) => {

                if (!entry.isIntersecting || !node.current) {
                    return;
                }
					
                setCurrentSrc(src);
		

                if (typeof onLazyLoad === "function") {
                    onLazyLoad(node.current);
                }
			});
		});

		// observer node

		node.current && observer.observe(node.current);

		//desconectar
		return () => {
			observer.disconnect();
		};
	}),
		[src, onLazyLoad];

	return (
		<img
			ref={node}
			src={currentSrc}
            {...imgProps}
		/>
	);
};
